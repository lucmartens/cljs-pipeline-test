# cljs-pipeline-test

```
IllegalStateException: Could not find where to put constant cljs$cst$keyword$event. Used by [poc.b.js, poc.a.js], selected common dep goog.style.style.js
```

Hypothesis:

- Constants cannot be put in `goog` modules (or is it any `.js` file?) because they do not have a `constants/*.cljs` input
- The file in which constants are placed is determined by finding the deepest common dependency between files sharing a constant
- The deepest common dependency between `poc.a` and `poc.b` in this case is `goog.style.style.js`

See [module graph](module_graph.txt)
